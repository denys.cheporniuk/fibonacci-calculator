import React, {
  useState,
  useEffect,
  useCallback,
  useMemo,
} from 'react';
import axios from 'axios';
import { IndexForm } from './IndexForm';
import { CalculatedValues } from './CalculatedValues';
import { SeenIndexes } from './SeenIndexes';

export const FibPage = () => {
  const [seenIndexes, setSeenIndexes] = useState([]);
  const [values, setValues] = useState({});

  const fetchValues = useCallback(async () => {
    const { data } = await axios.get('/api/values/current');
    setValues(data);
  }, []);

  const fetchIndexes = useCallback(async () => {
    const { data } = await axios.get('/api/values/all');
    setSeenIndexes(data);
  }, []);

  useEffect(() => {
    fetchValues();
    fetchIndexes();
  // eslint-disable-next-line
  }, []);

  const calculatedValues = useMemo(() => {
    const entries = [];

    for (let key in values) {
      entries.push({
        key: key,
        calculatedValue: values[key],
      })
    }

    return entries;
  }, [values]);

  return (
    <>
      <IndexForm />
      <SeenIndexes indexes={seenIndexes} />
      <CalculatedValues values={calculatedValues} />
    </>
  )
}
