import React from 'react';

export const CalculatedValues = ({ values }) => {
  return (
    <>
      <h3>Calculated values: </h3>
      {values.map((item) => (
        <div key={Math.random()}>
          {`For index ${item.key} I calculated ${item.calculatedValue}`}
        </div>
      ))}
    </>
  )
}
