import React from 'react';

export const SeenIndexes = ({ indexes }) => (
  <>
    <h3>Indexes I have seen: </h3>
    {indexes.map((item) => (
      <span key={Math.random()}>
        {item.number}
      </span>
    ))}
  </>
)
