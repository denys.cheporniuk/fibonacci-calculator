import React, { useState } from 'react';
import axios from 'axios';

export const IndexForm = () => {
  const [index, setIndex] = useState('');

  const inputHandler = (event) => {
    const { value } = event.target;

    setIndex(value);
  }

  const handleSubmit = async (event) => {
    event.preventDefault();

    await axios.post('/api/values', { index });

    setIndex('');
  }

  return (
    <form onSubmit={handleSubmit}>
      <label htmlFor="index">
        Enter your index:
      </label>
      <input
        id="index"
        type="text"
        value={index}
        onChange={inputHandler}
      />

      <button type="submit">
        Submit
      </button>
    </form>
  )
}
