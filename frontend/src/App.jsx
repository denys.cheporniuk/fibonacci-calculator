import React from 'react';
import { Routes, Route, NavLink } from "react-router-dom";
import { OtherPage } from "./components/OtherPage";
import { FibPage } from "./components/FibPage";

const App = () => {
  return (
    <div>
      <header>
        <nav>
          <NavLink to="/">Home</NavLink>
          <NavLink to="/otherpage">Other Page</NavLink>
        </nav>
      </header>
      <main>
        <Routes>
          <Route path="/" element={<FibPage />} />
          <Route path="/otherpage" element={<OtherPage />} />
        </Routes>
      </main>
    </div>
  );
}

export default App;
